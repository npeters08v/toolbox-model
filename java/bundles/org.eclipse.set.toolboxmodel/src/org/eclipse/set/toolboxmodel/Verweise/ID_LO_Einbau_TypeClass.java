/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID LO Einbau Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_LO_Einbau_TypeClass()
 * @model extendedMetaData="name='TCID_LO_Einbau' kind='elementOnly'"
 * @generated
 */
public interface ID_LO_Einbau_TypeClass extends Zeiger_TypeClass {
} // ID_LO_Einbau_TypeClass
