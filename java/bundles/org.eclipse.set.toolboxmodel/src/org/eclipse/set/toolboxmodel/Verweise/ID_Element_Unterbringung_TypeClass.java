/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Element Unterbringung Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Element_Unterbringung_TypeClass()
 * @model extendedMetaData="name='TCID_Element_Unterbringung' kind='elementOnly'"
 * @generated
 */
public interface ID_Element_Unterbringung_TypeClass extends Zeiger_TypeClass {
} // ID_Element_Unterbringung_TypeClass
