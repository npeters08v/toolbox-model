/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lf4 DV</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getLf4DV()
 * @model extendedMetaData="name='Lf_4_DV' kind='elementOnly'"
 * @generated
 */
public interface Lf4DV extends Signalbegriff_ID_TypeClass {
} // Lf4DV
