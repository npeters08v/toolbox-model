/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Fstr Zug Rangier Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Fstr_Zug_Rangier_TypeClass()
 * @model extendedMetaData="name='TCID_Fstr_Zug_Rangier' kind='elementOnly'"
 * @generated
 */
public interface ID_Fstr_Zug_Rangier_TypeClass extends Zeiger_TypeClass {
} // ID_Fstr_Zug_Rangier_TypeClass
