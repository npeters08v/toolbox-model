/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BSVRVA</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBSVRVA()
 * @model extendedMetaData="name='BS_vRVA' kind='elementOnly'"
 * @generated
 */
public interface BSVRVA extends Signalbegriff_ID_TypeClass {
} // BSVRVA
