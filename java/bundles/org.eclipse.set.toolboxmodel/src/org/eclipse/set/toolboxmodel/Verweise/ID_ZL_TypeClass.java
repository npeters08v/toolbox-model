/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID ZL Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_ZL_TypeClass()
 * @model extendedMetaData="name='TCID_ZL' kind='elementOnly'"
 * @generated
 */
public interface ID_ZL_TypeClass extends Zeiger_TypeClass {
} // ID_ZL_TypeClass
