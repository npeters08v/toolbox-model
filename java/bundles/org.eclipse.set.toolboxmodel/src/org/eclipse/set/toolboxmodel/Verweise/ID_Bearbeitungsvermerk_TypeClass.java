/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Bearbeitungsvermerk Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Bearbeitungsvermerk_TypeClass()
 * @model extendedMetaData="name='TCID_Bearbeitungsvermerk' kind='elementOnly'"
 * @generated
 */
public interface ID_Bearbeitungsvermerk_TypeClass extends Zeiger_TypeClass {
} // ID_Bearbeitungsvermerk_TypeClass
