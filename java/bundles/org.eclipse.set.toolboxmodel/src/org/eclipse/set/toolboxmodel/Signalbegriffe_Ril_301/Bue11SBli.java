/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue11 SBli</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBue11SBli()
 * @model extendedMetaData="name='Bue_1_1S_bli' kind='elementOnly'"
 * @generated
 */
public interface Bue11SBli extends Signalbegriff_ID_TypeClass {
} // Bue11SBli
