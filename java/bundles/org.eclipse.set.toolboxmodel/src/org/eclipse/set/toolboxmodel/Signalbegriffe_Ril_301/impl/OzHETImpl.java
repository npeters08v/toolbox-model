/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.OzHET;
import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.impl.Signalbegriff_ID_TypeClassImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Oz HET</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OzHETImpl extends Signalbegriff_ID_TypeClassImpl implements OzHET {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OzHETImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Signalbegriffe_Ril_301Package.eINSTANCE.getOzHET();
	}

} //OzHETImpl
