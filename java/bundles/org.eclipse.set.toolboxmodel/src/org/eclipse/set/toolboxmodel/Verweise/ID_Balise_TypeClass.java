/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Balise Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Balise_TypeClass()
 * @model extendedMetaData="name='TCID_Balise' kind='elementOnly'"
 * @generated
 */
public interface ID_Balise_TypeClass extends Zeiger_TypeClass {
} // ID_Balise_TypeClass
