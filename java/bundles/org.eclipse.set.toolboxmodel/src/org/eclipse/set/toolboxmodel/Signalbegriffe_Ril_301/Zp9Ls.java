/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Zp9 Ls</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getZp9Ls()
 * @model extendedMetaData="name='Zp_9_Ls' kind='elementOnly'"
 * @generated
 */
public interface Zp9Ls extends Signalbegriff_ID_TypeClass {
} // Zp9Ls
