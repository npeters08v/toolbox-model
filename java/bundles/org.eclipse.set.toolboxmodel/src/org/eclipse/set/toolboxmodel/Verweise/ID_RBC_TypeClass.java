/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID RBC Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_RBC_TypeClass()
 * @model extendedMetaData="name='TCID_RBC' kind='elementOnly'"
 * @generated
 */
public interface ID_RBC_TypeClass extends Zeiger_TypeClass {
} // ID_RBC_TypeClass
