/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_FMA_Anlage_Rangier_Frei_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID FMA Anlage Rangier Frei Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_FMA_Anlage_Rangier_Frei_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_FMA_Anlage_Rangier_Frei_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_FMA_Anlage_Rangier_Frei_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_FMA_ANLAGE_RANGIER_FREI_TYPE_CLASS;
	}

} //ID_FMA_Anlage_Rangier_Frei_TypeClassImpl
