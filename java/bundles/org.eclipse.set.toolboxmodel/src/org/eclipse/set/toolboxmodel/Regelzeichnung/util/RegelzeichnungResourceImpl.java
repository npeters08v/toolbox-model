/**
 */
package org.eclipse.set.toolboxmodel.Regelzeichnung.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.toolboxmodel.Regelzeichnung.util.RegelzeichnungResourceFactoryImpl
 * @generated
 */
public class RegelzeichnungResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public RegelzeichnungResourceImpl(URI uri) {
		super(uri);
	}

} //RegelzeichnungResourceImpl
