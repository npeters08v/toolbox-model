/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID ESTW Zentraleinheit Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_ESTW_Zentraleinheit_TypeClass()
 * @model extendedMetaData="name='TCID_ESTW_Zentraleinheit' kind='elementOnly'"
 * @generated
 */
public interface ID_ESTW_Zentraleinheit_TypeClass extends Zeiger_TypeClass {
} // ID_ESTW_Zentraleinheit_TypeClass
