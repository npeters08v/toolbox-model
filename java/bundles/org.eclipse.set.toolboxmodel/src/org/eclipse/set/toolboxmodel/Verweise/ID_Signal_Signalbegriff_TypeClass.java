/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Signal Signalbegriff Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Signal_Signalbegriff_TypeClass()
 * @model extendedMetaData="name='TCID_Signal_Signalbegriff' kind='elementOnly'"
 * @generated
 */
public interface ID_Signal_Signalbegriff_TypeClass extends Zeiger_TypeClass {
} // ID_Signal_Signalbegriff_TypeClass
