/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue12 Lp St</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBue12LpSt()
 * @model extendedMetaData="name='Bue_1_2Lp_st' kind='elementOnly'"
 * @generated
 */
public interface Bue12LpSt extends Signalbegriff_ID_TypeClass {
} // Bue12LpSt
