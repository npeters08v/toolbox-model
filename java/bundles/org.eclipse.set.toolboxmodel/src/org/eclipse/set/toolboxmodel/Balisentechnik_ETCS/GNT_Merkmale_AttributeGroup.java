/**
 */
package org.eclipse.set.toolboxmodel.Balisentechnik_ETCS;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GNT Merkmale Attribute Group</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Balisentechnik_ETCS.Balisentechnik_ETCSPackage#getGNT_Merkmale_AttributeGroup()
 * @model extendedMetaData="name='CGNT_Merkmale' kind='empty'"
 * @generated
 */
public interface GNT_Merkmale_AttributeGroup extends EObject {
} // GNT_Merkmale_AttributeGroup
