/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>El TAC</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getElTAC()
 * @model extendedMetaData="name='El_T_AC' kind='elementOnly'"
 * @generated
 */
public interface ElTAC extends Signalbegriff_ID_TypeClass {
} // ElTAC
