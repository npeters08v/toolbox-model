/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID BUE Anlage Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_BUE_Anlage_TypeClass()
 * @model extendedMetaData="name='TCID_BUE_Anlage' kind='elementOnly'"
 * @generated
 */
public interface ID_BUE_Anlage_TypeClass extends Zeiger_TypeClass {
} // ID_BUE_Anlage_TypeClass
