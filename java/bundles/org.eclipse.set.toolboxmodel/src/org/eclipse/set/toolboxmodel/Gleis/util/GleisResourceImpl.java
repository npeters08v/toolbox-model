/**
 */
package org.eclipse.set.toolboxmodel.Gleis.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.toolboxmodel.Gleis.util.GleisResourceFactoryImpl
 * @generated
 */
public class GleisResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public GleisResourceImpl(URI uri) {
		super(uri);
	}

} //GleisResourceImpl
