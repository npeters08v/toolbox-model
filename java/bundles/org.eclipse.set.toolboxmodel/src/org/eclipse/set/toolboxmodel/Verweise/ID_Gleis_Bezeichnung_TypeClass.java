/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Gleis Bezeichnung Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Gleis_Bezeichnung_TypeClass()
 * @model extendedMetaData="name='TCID_Gleis_Bezeichnung' kind='elementOnly'"
 * @generated
 */
public interface ID_Gleis_Bezeichnung_TypeClass extends Zeiger_TypeClass {
} // ID_Gleis_Bezeichnung_TypeClass
