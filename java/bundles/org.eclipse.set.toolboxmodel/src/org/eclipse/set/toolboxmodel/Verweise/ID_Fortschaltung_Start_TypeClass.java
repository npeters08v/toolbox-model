/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Fortschaltung Start Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Fortschaltung_Start_TypeClass()
 * @model extendedMetaData="name='TCID_Fortschaltung_Start' kind='elementOnly'"
 * @generated
 */
public interface ID_Fortschaltung_Start_TypeClass extends Zeiger_TypeClass {
} // ID_Fortschaltung_Start_TypeClass
