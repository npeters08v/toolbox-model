/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Zweites Haltfallkriterium Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Zweites_Haltfallkriterium_TypeClass()
 * @model extendedMetaData="name='TCID_Zweites_Haltfallkriterium' kind='elementOnly'"
 * @generated
 */
public interface ID_Zweites_Haltfallkriterium_TypeClass extends Zeiger_TypeClass {
} // ID_Zweites_Haltfallkriterium_TypeClass
