/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Hoehenpunkt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Hoehenpunkt_TypeClass()
 * @model extendedMetaData="name='TCID_Hoehenpunkt' kind='elementOnly'"
 * @generated
 */
public interface ID_Hoehenpunkt_TypeClass extends Zeiger_TypeClass {
} // ID_Hoehenpunkt_TypeClass
