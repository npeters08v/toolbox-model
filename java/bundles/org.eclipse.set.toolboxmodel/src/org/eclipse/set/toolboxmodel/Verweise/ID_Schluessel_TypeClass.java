/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Schluessel Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Schluessel_TypeClass()
 * @model extendedMetaData="name='TCID_Schluessel' kind='elementOnly'"
 * @generated
 */
public interface ID_Schluessel_TypeClass extends Zeiger_TypeClass {
} // ID_Schluessel_TypeClass
