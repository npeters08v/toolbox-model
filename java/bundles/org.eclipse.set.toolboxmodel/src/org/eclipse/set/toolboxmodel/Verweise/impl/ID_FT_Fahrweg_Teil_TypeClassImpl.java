/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_FT_Fahrweg_Teil_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID FT Fahrweg Teil Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_FT_Fahrweg_Teil_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_FT_Fahrweg_Teil_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_FT_Fahrweg_Teil_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_FT_FAHRWEG_TEIL_TYPE_CLASS;
	}

} //ID_FT_Fahrweg_Teil_TypeClassImpl
