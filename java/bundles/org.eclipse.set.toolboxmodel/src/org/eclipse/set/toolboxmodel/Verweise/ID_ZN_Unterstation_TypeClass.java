/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID ZN Unterstation Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_ZN_Unterstation_TypeClass()
 * @model extendedMetaData="name='TCID_ZN_Unterstation' kind='elementOnly'"
 * @generated
 */
public interface ID_ZN_Unterstation_TypeClass extends Zeiger_TypeClass {
} // ID_ZN_Unterstation_TypeClass
