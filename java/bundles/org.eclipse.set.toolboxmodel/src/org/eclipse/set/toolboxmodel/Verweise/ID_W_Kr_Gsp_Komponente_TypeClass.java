/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID WKr Gsp Komponente Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_W_Kr_Gsp_Komponente_TypeClass()
 * @model extendedMetaData="name='TCID_W_Kr_Gsp_Komponente' kind='elementOnly'"
 * @generated
 */
public interface ID_W_Kr_Gsp_Komponente_TypeClass extends Zeiger_TypeClass {
} // ID_W_Kr_Gsp_Komponente_TypeClass
