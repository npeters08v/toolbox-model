/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue00 Lp</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBue00Lp()
 * @model extendedMetaData="name='Bue_0_0Lp' kind='elementOnly'"
 * @generated
 */
public interface Bue00Lp extends Signalbegriff_ID_TypeClass {
} // Bue00Lp
