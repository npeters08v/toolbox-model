/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Verknuepftes Element Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Verknuepftes_Element_TypeClass()
 * @model extendedMetaData="name='TCID_Verknuepftes_Element' kind='elementOnly'"
 * @generated
 */
public interface ID_Verknuepftes_Element_TypeClass extends Zeiger_TypeClass {
} // ID_Verknuepftes_Element_TypeClass
