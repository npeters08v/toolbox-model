/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Regelzeichnung Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Regelzeichnung_TypeClass()
 * @model extendedMetaData="name='TCID_Regelzeichnung' kind='elementOnly'"
 * @generated
 */
public interface ID_Regelzeichnung_TypeClass extends Zeiger_TypeClass {
} // ID_Regelzeichnung_TypeClass
