/**
 */
package org.eclipse.set.toolboxmodel.Balisentechnik_ETCS;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ZUB Bereichsgrenze Nach GNT Attribute Group</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Balisentechnik_ETCS.Balisentechnik_ETCSPackage#getZUB_Bereichsgrenze_Nach_GNT_AttributeGroup()
 * @model extendedMetaData="name='CZUB_Bereichsgrenze_Nach_GNT' kind='empty'"
 * @generated
 */
public interface ZUB_Bereichsgrenze_Nach_GNT_AttributeGroup extends EObject {
} // ZUB_Bereichsgrenze_Nach_GNT_AttributeGroup
