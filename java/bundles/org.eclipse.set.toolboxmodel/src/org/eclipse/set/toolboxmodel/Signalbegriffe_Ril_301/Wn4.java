/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wn4</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getWn4()
 * @model extendedMetaData="name='Wn_4' kind='elementOnly'"
 * @generated
 */
public interface Wn4 extends Signalbegriff_ID_TypeClass {
} // Wn4
