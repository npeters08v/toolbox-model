/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ms Ws2sw P</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getMsWs2swP()
 * @model extendedMetaData="name='Ms_ws_2swP' kind='elementOnly'"
 * @generated
 */
public interface MsWs2swP extends Signalbegriff_ID_TypeClass {
} // MsWs2swP
