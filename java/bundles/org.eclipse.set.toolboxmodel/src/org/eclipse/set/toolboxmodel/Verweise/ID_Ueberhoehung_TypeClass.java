/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Ueberhoehung Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Ueberhoehung_TypeClass()
 * @model extendedMetaData="name='TCID_Ueberhoehung' kind='elementOnly'"
 * @generated
 */
public interface ID_Ueberhoehung_TypeClass extends Zeiger_TypeClass {
} // ID_Ueberhoehung_TypeClass
