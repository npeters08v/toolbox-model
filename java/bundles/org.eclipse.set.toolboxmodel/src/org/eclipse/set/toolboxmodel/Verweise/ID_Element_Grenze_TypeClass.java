/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Element Grenze Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Element_Grenze_TypeClass()
 * @model extendedMetaData="name='TCID_Element_Grenze' kind='elementOnly'"
 * @generated
 */
public interface ID_Element_Grenze_TypeClass extends Zeiger_TypeClass {
} // ID_Element_Grenze_TypeClass
