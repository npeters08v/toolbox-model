/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Beginn Bereich Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Beginn_Bereich_TypeClass()
 * @model extendedMetaData="name='TCID_Beginn_Bereich' kind='elementOnly'"
 * @generated
 */
public interface ID_Beginn_Bereich_TypeClass extends Zeiger_TypeClass {
} // ID_Beginn_Bereich_TypeClass
