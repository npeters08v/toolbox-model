/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID EV Modul Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_EV_Modul_TypeClass()
 * @model extendedMetaData="name='TCID_EV_Modul' kind='elementOnly'"
 * @generated
 */
public interface ID_EV_Modul_TypeClass extends Zeiger_TypeClass {
} // ID_EV_Modul_TypeClass
