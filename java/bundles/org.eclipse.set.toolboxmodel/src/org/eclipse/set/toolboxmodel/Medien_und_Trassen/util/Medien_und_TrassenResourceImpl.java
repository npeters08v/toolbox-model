/**
 */
package org.eclipse.set.toolboxmodel.Medien_und_Trassen.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.toolboxmodel.Medien_und_Trassen.util.Medien_und_TrassenResourceFactoryImpl
 * @generated
 */
public class Medien_und_TrassenResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Medien_und_TrassenResourceImpl(URI uri) {
		super(uri);
	}

} //Medien_und_TrassenResourceImpl
