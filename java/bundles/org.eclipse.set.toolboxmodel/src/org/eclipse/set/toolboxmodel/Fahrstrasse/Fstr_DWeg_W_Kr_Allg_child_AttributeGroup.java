/**
 */
package org.eclipse.set.toolboxmodel.Fahrstrasse;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fstr DWeg WKr Allg child Attribute Group</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Fahrstrasse.FahrstrassePackage#getFstr_DWeg_W_Kr_Allg_child_AttributeGroup()
 * @model extendedMetaData="name='CFstr_DWeg_W_Kr_Allg_child' kind='elementOnly'"
 * @generated
 */
public interface Fstr_DWeg_W_Kr_Allg_child_AttributeGroup extends Fstr_DWeg_W_Kr_Allg_AttributeGroup {
} // Fstr_DWeg_W_Kr_Allg_child_AttributeGroup
