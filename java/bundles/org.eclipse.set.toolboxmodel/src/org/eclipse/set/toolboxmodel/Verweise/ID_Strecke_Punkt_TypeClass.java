/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Strecke Punkt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Strecke_Punkt_TypeClass()
 * @model extendedMetaData="name='TCID_Strecke_Punkt' kind='elementOnly'"
 * @generated
 */
public interface ID_Strecke_Punkt_TypeClass extends Zeiger_TypeClass {
} // ID_Strecke_Punkt_TypeClass
