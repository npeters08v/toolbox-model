/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_W_Kr_Anlage_ohne_Proxy_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID WKr Anlage ohne Proxy Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_W_Kr_Anlage_ohne_Proxy_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_W_Kr_Anlage_ohne_Proxy_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_W_Kr_Anlage_ohne_Proxy_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_WKR_ANLAGE_OHNE_PROXY_TYPE_CLASS;
	}

} //ID_W_Kr_Anlage_ohne_Proxy_TypeClassImpl
