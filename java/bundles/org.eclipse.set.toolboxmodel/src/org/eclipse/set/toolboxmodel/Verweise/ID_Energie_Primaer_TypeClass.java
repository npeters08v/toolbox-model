/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Energie Primaer Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Energie_Primaer_TypeClass()
 * @model extendedMetaData="name='TCID_Energie_Primaer' kind='elementOnly'"
 * @generated
 */
public interface ID_Energie_Primaer_TypeClass extends Zeiger_TypeClass {
} // ID_Energie_Primaer_TypeClass
