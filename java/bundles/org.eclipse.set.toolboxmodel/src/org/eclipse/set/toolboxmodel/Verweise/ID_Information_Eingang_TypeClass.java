/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Information Eingang Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Information_Eingang_TypeClass()
 * @model extendedMetaData="name='TCID_Information_Eingang' kind='elementOnly'"
 * @generated
 */
public interface ID_Information_Eingang_TypeClass extends Zeiger_TypeClass {
} // ID_Information_Eingang_TypeClass
