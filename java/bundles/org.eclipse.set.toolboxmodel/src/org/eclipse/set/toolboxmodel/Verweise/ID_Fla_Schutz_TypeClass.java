/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Fla Schutz Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Fla_Schutz_TypeClass()
 * @model extendedMetaData="name='TCID_Fla_Schutz' kind='elementOnly'"
 * @generated
 */
public interface ID_Fla_Schutz_TypeClass extends Zeiger_TypeClass {
} // ID_Fla_Schutz_TypeClass
