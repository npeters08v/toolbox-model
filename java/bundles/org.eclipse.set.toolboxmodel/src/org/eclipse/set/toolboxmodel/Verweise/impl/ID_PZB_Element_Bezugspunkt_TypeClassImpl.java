/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_PZB_Element_Bezugspunkt_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID PZB Element Bezugspunkt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_PZB_Element_Bezugspunkt_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_PZB_Element_Bezugspunkt_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_PZB_Element_Bezugspunkt_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_PZB_ELEMENT_BEZUGSPUNKT_TYPE_CLASS;
	}

} //ID_PZB_Element_Bezugspunkt_TypeClassImpl
