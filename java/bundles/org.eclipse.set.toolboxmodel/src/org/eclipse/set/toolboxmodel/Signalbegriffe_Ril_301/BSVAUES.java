/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BSVAUES</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBSVAUES()
 * @model extendedMetaData="name='BS_vA_UES' kind='elementOnly'"
 * @generated
 */
public interface BSVAUES extends Signalbegriff_ID_TypeClass {
} // BSVAUES
