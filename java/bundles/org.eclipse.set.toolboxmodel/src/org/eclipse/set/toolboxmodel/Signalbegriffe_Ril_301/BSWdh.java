/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BS Wdh</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBSWdh()
 * @model extendedMetaData="name='BS_Wdh' kind='elementOnly'"
 * @generated
 */
public interface BSWdh extends Signalbegriff_ID_TypeClass {
} // BSWdh
