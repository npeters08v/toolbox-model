/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Planung Einzel Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Planung_Einzel_TypeClass()
 * @model extendedMetaData="name='TCID_Planung_Einzel' kind='elementOnly'"
 * @generated
 */
public interface ID_Planung_Einzel_TypeClass extends Zeiger_TypeClass {
} // ID_Planung_Einzel_TypeClass
