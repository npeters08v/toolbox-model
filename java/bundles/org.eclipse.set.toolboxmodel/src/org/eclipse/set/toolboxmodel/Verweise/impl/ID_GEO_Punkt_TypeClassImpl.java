/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_GEO_Punkt_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID GEO Punkt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_GEO_Punkt_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_GEO_Punkt_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_GEO_Punkt_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_GEO_PUNKT_TYPE_CLASS;
	}

} //ID_GEO_Punkt_TypeClassImpl
