/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Fstr DWeg Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Fstr_DWeg_TypeClass()
 * @model extendedMetaData="name='TCID_Fstr_DWeg' kind='elementOnly'"
 * @generated
 */
public interface ID_Fstr_DWeg_TypeClass extends Zeiger_TypeClass {
} // ID_Fstr_DWeg_TypeClass
