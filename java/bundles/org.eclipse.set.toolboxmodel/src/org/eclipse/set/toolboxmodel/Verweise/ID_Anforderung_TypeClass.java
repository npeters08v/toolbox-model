/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Anforderung Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Anforderung_TypeClass()
 * @model extendedMetaData="name='TCID_Anforderung' kind='elementOnly'"
 * @generated
 */
public interface ID_Anforderung_TypeClass extends Zeiger_TypeClass {
} // ID_Anforderung_TypeClass
