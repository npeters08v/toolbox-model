/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Schaltmittel Zuordnung Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Schaltmittel_Zuordnung_TypeClass()
 * @model extendedMetaData="name='TCID_Schaltmittel_Zuordnung' kind='elementOnly'"
 * @generated
 */
public interface ID_Schaltmittel_Zuordnung_TypeClass extends Zeiger_TypeClass {
} // ID_Schaltmittel_Zuordnung_TypeClass
