/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ms Sk Ge</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getMsSkGe()
 * @model extendedMetaData="name='Ms_Sk_ge' kind='elementOnly'"
 * @generated
 */
public interface MsSkGe extends Signalbegriff_ID_TypeClass {
} // MsSkGe
