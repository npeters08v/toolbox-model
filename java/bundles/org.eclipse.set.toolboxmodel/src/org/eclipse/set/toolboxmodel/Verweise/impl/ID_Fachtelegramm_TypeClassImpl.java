/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_Fachtelegramm_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID Fachtelegramm Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_Fachtelegramm_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_Fachtelegramm_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_Fachtelegramm_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_FACHTELEGRAMM_TYPE_CLASS;
	}

} //ID_Fachtelegramm_TypeClassImpl
