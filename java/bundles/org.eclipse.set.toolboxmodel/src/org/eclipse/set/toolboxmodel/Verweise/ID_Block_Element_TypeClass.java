/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Block Element Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Block_Element_TypeClass()
 * @model extendedMetaData="name='TCID_Block_Element' kind='elementOnly'"
 * @generated
 */
public interface ID_Block_Element_TypeClass extends Zeiger_TypeClass {
} // ID_Block_Element_TypeClass
