/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue23 R</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBue23R()
 * @model extendedMetaData="name='Bue_2_3R' kind='elementOnly'"
 * @generated
 */
public interface Bue23R extends Signalbegriff_ID_TypeClass {
} // Bue23R
