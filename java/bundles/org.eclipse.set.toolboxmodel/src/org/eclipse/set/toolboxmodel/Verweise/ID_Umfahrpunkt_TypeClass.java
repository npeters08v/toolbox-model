/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Umfahrpunkt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Umfahrpunkt_TypeClass()
 * @model extendedMetaData="name='TCID_Umfahrpunkt' kind='elementOnly'"
 * @generated
 */
public interface ID_Umfahrpunkt_TypeClass extends Zeiger_TypeClass {
} // ID_Umfahrpunkt_TypeClass
