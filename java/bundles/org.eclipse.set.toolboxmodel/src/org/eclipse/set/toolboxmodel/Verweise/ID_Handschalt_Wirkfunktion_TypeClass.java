/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Handschalt Wirkfunktion Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Handschalt_Wirkfunktion_TypeClass()
 * @model extendedMetaData="name='TCID_Handschalt_Wirkfunktion' kind='elementOnly'"
 * @generated
 */
public interface ID_Handschalt_Wirkfunktion_TypeClass extends Zeiger_TypeClass {
} // ID_Handschalt_Wirkfunktion_TypeClass
