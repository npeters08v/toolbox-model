/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Bedien Bezirk Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Bedien_Bezirk_TypeClass()
 * @model extendedMetaData="name='TCID_Bedien_Bezirk' kind='elementOnly'"
 * @generated
 */
public interface ID_Bedien_Bezirk_TypeClass extends Zeiger_TypeClass {
} // ID_Bedien_Bezirk_TypeClass
