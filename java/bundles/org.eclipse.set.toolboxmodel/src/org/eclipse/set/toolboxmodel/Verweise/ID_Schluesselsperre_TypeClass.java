/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Schluesselsperre Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Schluesselsperre_TypeClass()
 * @model extendedMetaData="name='TCID_Schluesselsperre' kind='elementOnly'"
 * @generated
 */
public interface ID_Schluesselsperre_TypeClass extends Zeiger_TypeClass {
} // ID_Schluesselsperre_TypeClass
