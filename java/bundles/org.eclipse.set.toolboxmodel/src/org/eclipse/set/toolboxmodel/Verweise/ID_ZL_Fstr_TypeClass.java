/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID ZL Fstr Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_ZL_Fstr_TypeClass()
 * @model extendedMetaData="name='TCID_ZL_Fstr' kind='elementOnly'"
 * @generated
 */
public interface ID_ZL_Fstr_TypeClass extends Zeiger_TypeClass {
} // ID_ZL_Fstr_TypeClass
