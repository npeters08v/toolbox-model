/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Oz OB Grenze</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getOzOBGrenze()
 * @model extendedMetaData="name='Oz_OB_Grenze' kind='elementOnly'"
 * @generated
 */
public interface OzOBGrenze extends Signalbegriff_ID_TypeClass {
} // OzOBGrenze
