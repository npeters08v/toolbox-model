/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_Markante_Stelle_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID Markante Stelle Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_Markante_Stelle_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_Markante_Stelle_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_Markante_Stelle_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_MARKANTE_STELLE_TYPE_CLASS;
	}

} //ID_Markante_Stelle_TypeClassImpl
