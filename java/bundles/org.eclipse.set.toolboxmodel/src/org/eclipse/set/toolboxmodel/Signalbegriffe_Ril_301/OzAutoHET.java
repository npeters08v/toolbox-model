/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Oz Auto HET</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getOzAutoHET()
 * @model extendedMetaData="name='Oz_Auto_HET' kind='elementOnly'"
 * @generated
 */
public interface OzAutoHET extends Signalbegriff_ID_TypeClass {
} // OzAutoHET
