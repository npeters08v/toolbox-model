/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Ur Objekt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Ur_Objekt_TypeClass()
 * @model extendedMetaData="name='TCID_Ur_Objekt' kind='elementOnly'"
 * @generated
 */
public interface ID_Ur_Objekt_TypeClass extends Zeiger_TypeClass {
} // ID_Ur_Objekt_TypeClass
