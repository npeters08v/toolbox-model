/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Basis Objekt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Basis_Objekt_TypeClass()
 * @model extendedMetaData="name='TCID_Basis_Objekt' kind='elementOnly'"
 * @generated
 */
public interface ID_Basis_Objekt_TypeClass extends Zeiger_TypeClass {
} // ID_Basis_Objekt_TypeClass
