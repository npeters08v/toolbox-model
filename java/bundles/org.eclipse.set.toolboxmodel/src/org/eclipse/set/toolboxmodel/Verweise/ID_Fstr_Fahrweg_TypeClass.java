/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Fstr Fahrweg Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Fstr_Fahrweg_TypeClass()
 * @model extendedMetaData="name='TCID_Fstr_Fahrweg' kind='elementOnly'"
 * @generated
 */
public interface ID_Fstr_Fahrweg_TypeClass extends Zeiger_TypeClass {
} // ID_Fstr_Fahrweg_TypeClass
