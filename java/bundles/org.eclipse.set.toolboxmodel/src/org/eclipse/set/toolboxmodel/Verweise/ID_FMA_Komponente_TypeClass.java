/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID FMA Komponente Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_FMA_Komponente_TypeClass()
 * @model extendedMetaData="name='TCID_FMA_Komponente' kind='elementOnly'"
 * @generated
 */
public interface ID_FMA_Komponente_TypeClass extends Zeiger_TypeClass {
} // ID_FMA_Komponente_TypeClass
