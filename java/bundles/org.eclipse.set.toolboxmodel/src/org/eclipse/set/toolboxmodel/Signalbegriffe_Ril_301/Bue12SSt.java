/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue12 SSt</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBue12SSt()
 * @model extendedMetaData="name='Bue_1_2S_st' kind='elementOnly'"
 * @generated
 */
public interface Bue12SSt extends Signalbegriff_ID_TypeClass {
} // Bue12SSt
