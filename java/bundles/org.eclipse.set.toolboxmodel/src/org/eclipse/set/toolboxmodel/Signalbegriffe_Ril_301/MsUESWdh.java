/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ms UES Wdh</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getMsUESWdh()
 * @model extendedMetaData="name='Ms_UES_Wdh' kind='elementOnly'"
 * @generated
 */
public interface MsUESWdh extends Signalbegriff_ID_TypeClass {
} // MsUESWdh
