/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vr1</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getVr1()
 * @model extendedMetaData="name='Vr_1' kind='elementOnly'"
 * @generated
 */
public interface Vr1 extends Signalbegriff_ID_TypeClass {
} // Vr1
