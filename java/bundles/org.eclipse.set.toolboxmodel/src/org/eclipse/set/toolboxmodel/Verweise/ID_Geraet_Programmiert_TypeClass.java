/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Geraet Programmiert Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Geraet_Programmiert_TypeClass()
 * @model extendedMetaData="name='TCID_Geraet_Programmiert' kind='elementOnly'"
 * @generated
 */
public interface ID_Geraet_Programmiert_TypeClass extends Zeiger_TypeClass {
} // ID_Geraet_Programmiert_TypeClass
