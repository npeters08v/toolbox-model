/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue11 Lp Bli</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBue11LpBli()
 * @model extendedMetaData="name='Bue_1_1Lp_bli' kind='elementOnly'"
 * @generated
 */
public interface Bue11LpBli extends Signalbegriff_ID_TypeClass {
} // Bue11LpBli
