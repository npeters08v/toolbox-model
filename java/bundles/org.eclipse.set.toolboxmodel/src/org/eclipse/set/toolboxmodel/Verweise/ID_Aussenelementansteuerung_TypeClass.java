/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Aussenelementansteuerung Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Aussenelementansteuerung_TypeClass()
 * @model extendedMetaData="name='TCID_Aussenelementansteuerung' kind='elementOnly'"
 * @generated
 */
public interface ID_Aussenelementansteuerung_TypeClass extends Zeiger_TypeClass {
} // ID_Aussenelementansteuerung_TypeClass
