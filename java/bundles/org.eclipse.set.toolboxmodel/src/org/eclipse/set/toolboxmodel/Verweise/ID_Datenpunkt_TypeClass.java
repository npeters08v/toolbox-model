/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Datenpunkt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Datenpunkt_TypeClass()
 * @model extendedMetaData="name='TCID_Datenpunkt' kind='elementOnly'"
 * @generated
 */
public interface ID_Datenpunkt_TypeClass extends Zeiger_TypeClass {
} // ID_Datenpunkt_TypeClass
