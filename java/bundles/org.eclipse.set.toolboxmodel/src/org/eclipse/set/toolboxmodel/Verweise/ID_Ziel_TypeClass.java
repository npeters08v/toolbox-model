/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Ziel Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Ziel_TypeClass()
 * @model extendedMetaData="name='TCID_Ziel' kind='elementOnly'"
 * @generated
 */
public interface ID_Ziel_TypeClass extends Zeiger_TypeClass {
} // ID_Ziel_TypeClass
