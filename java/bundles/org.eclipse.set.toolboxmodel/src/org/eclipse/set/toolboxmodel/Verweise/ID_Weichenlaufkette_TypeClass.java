/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Weichenlaufkette Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Weichenlaufkette_TypeClass()
 * @model extendedMetaData="name='TCID_Weichenlaufkette' kind='elementOnly'"
 * @generated
 */
public interface ID_Weichenlaufkette_TypeClass extends Zeiger_TypeClass {
} // ID_Weichenlaufkette_TypeClass
