/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.util.Signalbegriffe_Ril_301ResourceFactoryImpl
 * @generated
 */
public class Signalbegriffe_Ril_301ResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Signalbegriffe_Ril_301ResourceImpl(URI uri) {
		super(uri);
	}

} //Signalbegriffe_Ril_301ResourceImpl
