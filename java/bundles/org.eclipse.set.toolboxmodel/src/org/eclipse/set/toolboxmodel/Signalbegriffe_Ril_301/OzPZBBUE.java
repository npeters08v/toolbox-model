/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Oz PZBBUE</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getOzPZBBUE()
 * @model extendedMetaData="name='Oz_PZB_BUE' kind='elementOnly'"
 * @generated
 */
public interface OzPZBBUE extends Signalbegriff_ID_TypeClass {
} // OzPZBBUE
