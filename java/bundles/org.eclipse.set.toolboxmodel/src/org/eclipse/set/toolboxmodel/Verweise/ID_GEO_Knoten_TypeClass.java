/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID GEO Knoten Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_GEO_Knoten_TypeClass()
 * @model extendedMetaData="name='TCID_GEO_Knoten' kind='elementOnly'"
 * @generated
 */
public interface ID_GEO_Knoten_TypeClass extends Zeiger_TypeClass {
} // ID_GEO_Knoten_TypeClass
