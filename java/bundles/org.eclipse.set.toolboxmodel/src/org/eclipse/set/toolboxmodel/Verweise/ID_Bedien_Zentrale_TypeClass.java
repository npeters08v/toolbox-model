/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Bedien Zentrale Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Bedien_Zentrale_TypeClass()
 * @model extendedMetaData="name='TCID_Bedien_Zentrale' kind='elementOnly'"
 * @generated
 */
public interface ID_Bedien_Zentrale_TypeClass extends Zeiger_TypeClass {
} // ID_Bedien_Zentrale_TypeClass
