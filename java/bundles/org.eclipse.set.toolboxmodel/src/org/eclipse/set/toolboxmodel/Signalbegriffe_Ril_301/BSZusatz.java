/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BS Zusatz</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBSZusatz()
 * @model extendedMetaData="name='BS_Zusatz' kind='elementOnly'"
 * @generated
 */
public interface BSZusatz extends Signalbegriff_ID_TypeClass {
} // BSZusatz
