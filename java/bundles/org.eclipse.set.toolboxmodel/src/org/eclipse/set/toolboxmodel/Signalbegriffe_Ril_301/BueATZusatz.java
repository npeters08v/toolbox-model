/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue AT Zusatz</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBueATZusatz()
 * @model extendedMetaData="name='Bue_AT_Zusatz' kind='empty'"
 * @generated
 */
public interface BueATZusatz extends EObject {
} // BueATZusatz
