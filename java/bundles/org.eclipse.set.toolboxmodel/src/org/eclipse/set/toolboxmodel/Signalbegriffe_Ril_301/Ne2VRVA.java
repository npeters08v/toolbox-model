/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ne2 VRVA</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getNe2VRVA()
 * @model extendedMetaData="name='Ne_2_vRVA' kind='elementOnly'"
 * @generated
 */
public interface Ne2VRVA extends Signalbegriff_ID_TypeClass {
} // Ne2VRVA
