/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package;
import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Sv0;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.impl.Signalbegriff_ID_TypeClassImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sv0</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Sv0Impl extends Signalbegriff_ID_TypeClassImpl implements Sv0 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Sv0Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Signalbegriffe_Ril_301Package.eINSTANCE.getSv0();
	}

} //Sv0Impl
