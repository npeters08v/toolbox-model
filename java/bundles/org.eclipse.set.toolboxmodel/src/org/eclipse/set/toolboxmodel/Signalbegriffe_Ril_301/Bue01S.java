/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue01 S</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBue01S()
 * @model extendedMetaData="name='Bue_0_1S' kind='elementOnly'"
 * @generated
 */
public interface Bue01S extends Signalbegriff_ID_TypeClass {
} // Bue01S
