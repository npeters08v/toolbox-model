/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID GFR Anlage ohne Proxy Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_GFR_Anlage_ohne_Proxy_TypeClass()
 * @model extendedMetaData="name='TCID_GFR_Anlage_ohne_Proxy' kind='elementOnly'"
 * @generated
 */
public interface ID_GFR_Anlage_ohne_Proxy_TypeClass extends Zeiger_TypeClass {
} // ID_GFR_Anlage_ohne_Proxy_TypeClass
