/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID WKr Anlage Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_W_Kr_Anlage_TypeClass()
 * @model extendedMetaData="name='TCID_W_Kr_Anlage' kind='elementOnly'"
 * @generated
 */
public interface ID_W_Kr_Anlage_TypeClass extends Zeiger_TypeClass {
} // ID_W_Kr_Anlage_TypeClass
