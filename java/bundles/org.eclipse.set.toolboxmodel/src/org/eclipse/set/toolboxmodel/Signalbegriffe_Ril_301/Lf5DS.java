/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lf5 DS</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getLf5DS()
 * @model extendedMetaData="name='Lf_5_DS' kind='elementOnly'"
 * @generated
 */
public interface Lf5DS extends Signalbegriff_ID_TypeClass {
} // Lf5DS
