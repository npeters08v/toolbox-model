/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID NB Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_NB_TypeClass()
 * @model extendedMetaData="name='TCID_NB' kind='elementOnly'"
 * @generated
 */
public interface ID_NB_TypeClass extends Zeiger_TypeClass {
} // ID_NB_TypeClass
