/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_ETCS_Knoten_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID ETCS Knoten Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_ETCS_Knoten_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_ETCS_Knoten_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_ETCS_Knoten_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_ETCS_KNOTEN_TYPE_CLASS;
	}

} //ID_ETCS_Knoten_TypeClassImpl
