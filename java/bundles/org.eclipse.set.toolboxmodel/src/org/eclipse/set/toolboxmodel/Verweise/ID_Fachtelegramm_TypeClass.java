/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Fachtelegramm Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Fachtelegramm_TypeClass()
 * @model extendedMetaData="name='TCID_Fachtelegramm' kind='elementOnly'"
 * @generated
 */
public interface ID_Fachtelegramm_TypeClass extends Zeiger_TypeClass {
} // ID_Fachtelegramm_TypeClass
