/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Trasse Knoten Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Trasse_Knoten_TypeClass()
 * @model extendedMetaData="name='TCID_Trasse_Knoten' kind='elementOnly'"
 * @generated
 */
public interface ID_Trasse_Knoten_TypeClass extends Zeiger_TypeClass {
} // ID_Trasse_Knoten_TypeClass
