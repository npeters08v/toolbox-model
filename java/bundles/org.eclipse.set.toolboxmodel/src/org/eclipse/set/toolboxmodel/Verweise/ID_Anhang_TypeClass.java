/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Anhang Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Anhang_TypeClass()
 * @model extendedMetaData="name='TCID_Anhang' kind='elementOnly'"
 * @generated
 */
public interface ID_Anhang_TypeClass extends Zeiger_TypeClass {
} // ID_Anhang_TypeClass
