/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Stellwerk Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Stellwerk_TypeClass()
 * @model extendedMetaData="name='TCID_Stellwerk' kind='elementOnly'"
 * @generated
 */
public interface ID_Stellwerk_TypeClass extends Zeiger_TypeClass {
} // ID_Stellwerk_TypeClass
