/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ms Ws Sw Ws</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getMsWsSwWs()
 * @model extendedMetaData="name='Ms_ws_sw_ws' kind='elementOnly'"
 * @generated
 */
public interface MsWsSwWs extends Signalbegriff_ID_TypeClass {
} // MsWsSwWs
