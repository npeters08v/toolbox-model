/**
 */
package org.eclipse.set.toolboxmodel.Ansteuerung_Element.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.toolboxmodel.Ansteuerung_Element.util.Ansteuerung_ElementResourceFactoryImpl
 * @generated
 */
public class Ansteuerung_ElementResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Ansteuerung_ElementResourceImpl(URI uri) {
		super(uri);
	}

} //Ansteuerung_ElementResourceImpl
