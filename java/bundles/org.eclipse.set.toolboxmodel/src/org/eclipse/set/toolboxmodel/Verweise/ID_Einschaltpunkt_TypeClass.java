/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Einschaltpunkt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Einschaltpunkt_TypeClass()
 * @model extendedMetaData="name='TCID_Einschaltpunkt' kind='elementOnly'"
 * @generated
 */
public interface ID_Einschaltpunkt_TypeClass extends Zeiger_TypeClass {
} // ID_Einschaltpunkt_TypeClass
