/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sv WPf</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getSvWPf()
 * @model extendedMetaData="name='Sv_wPf' kind='elementOnly'"
 * @generated
 */
public interface SvWPf extends Signalbegriff_ID_TypeClass {
} // SvWPf
