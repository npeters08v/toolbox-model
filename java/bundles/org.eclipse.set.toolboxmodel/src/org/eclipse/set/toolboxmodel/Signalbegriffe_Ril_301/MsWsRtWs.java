/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ms Ws Rt Ws</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getMsWsRtWs()
 * @model extendedMetaData="name='Ms_ws_rt_ws' kind='elementOnly'"
 * @generated
 */
public interface MsWsRtWs extends Signalbegriff_ID_TypeClass {
} // MsWsRtWs
