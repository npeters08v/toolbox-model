/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Strecke Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Strecke_TypeClass()
 * @model extendedMetaData="name='TCID_Strecke' kind='elementOnly'"
 * @generated
 */
public interface ID_Strecke_TypeClass extends Zeiger_TypeClass {
} // ID_Strecke_TypeClass
