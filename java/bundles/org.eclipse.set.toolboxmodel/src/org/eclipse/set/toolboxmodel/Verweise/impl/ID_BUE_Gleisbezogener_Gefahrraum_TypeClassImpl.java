/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_BUE_Gleisbezogener_Gefahrraum_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID BUE Gleisbezogener Gefahrraum Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_BUE_Gleisbezogener_Gefahrraum_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_BUE_Gleisbezogener_Gefahrraum_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_BUE_Gleisbezogener_Gefahrraum_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_BUE_GLEISBEZOGENER_GEFAHRRAUM_TYPE_CLASS;
	}

} //ID_BUE_Gleisbezogener_Gefahrraum_TypeClassImpl
