/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Trasse Kante Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Trasse_Kante_TypeClass()
 * @model extendedMetaData="name='TCID_Trasse_Kante' kind='elementOnly'"
 * @generated
 */
public interface ID_Trasse_Kante_TypeClass extends Zeiger_TypeClass {
} // ID_Trasse_Kante_TypeClass
