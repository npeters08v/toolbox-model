/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue10 Lp Bli</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBue10LpBli()
 * @model extendedMetaData="name='Bue_1_0Lp_bli' kind='elementOnly'"
 * @generated
 */
public interface Bue10LpBli extends Signalbegriff_ID_TypeClass {
} // Bue10LpBli
