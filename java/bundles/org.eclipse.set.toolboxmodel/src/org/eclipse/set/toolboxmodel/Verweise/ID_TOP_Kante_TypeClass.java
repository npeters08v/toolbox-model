/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID TOP Kante Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_TOP_Kante_TypeClass()
 * @model extendedMetaData="name='TCID_TOP_Kante' kind='elementOnly'"
 * @generated
 */
public interface ID_TOP_Kante_TypeClass extends Zeiger_TypeClass {
} // ID_TOP_Kante_TypeClass
