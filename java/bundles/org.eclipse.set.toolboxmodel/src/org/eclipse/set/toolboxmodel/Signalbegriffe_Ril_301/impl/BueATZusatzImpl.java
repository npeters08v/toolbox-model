/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.BueATZusatz;
import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bue AT Zusatz</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BueATZusatzImpl extends MinimalEObjectImpl.Container implements BueATZusatz {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BueATZusatzImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Signalbegriffe_Ril_301Package.eINSTANCE.getBueATZusatz();
	}

} //BueATZusatzImpl
