/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID ZN Fortschalt Kriterium Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_ZN_Fortschalt_Kriterium_TypeClass()
 * @model extendedMetaData="name='TCID_ZN_Fortschalt_Kriterium' kind='elementOnly'"
 * @generated
 */
public interface ID_ZN_Fortschalt_Kriterium_TypeClass extends Zeiger_TypeClass {
} // ID_ZN_Fortschalt_Kriterium_TypeClass
