/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID PZB Element Bezugspunkt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_PZB_Element_Bezugspunkt_TypeClass()
 * @model extendedMetaData="name='TCID_PZB_Element_Bezugspunkt' kind='elementOnly'"
 * @generated
 */
public interface ID_PZB_Element_Bezugspunkt_TypeClass extends Zeiger_TypeClass {
} // ID_PZB_Element_Bezugspunkt_TypeClass
