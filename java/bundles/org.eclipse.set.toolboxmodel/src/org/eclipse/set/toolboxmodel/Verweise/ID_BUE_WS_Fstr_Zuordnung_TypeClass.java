/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID BUE WS Fstr Zuordnung Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_BUE_WS_Fstr_Zuordnung_TypeClass()
 * @model extendedMetaData="name='TCID_BUE_WS_Fstr_Zuordnung' kind='elementOnly'"
 * @generated
 */
public interface ID_BUE_WS_Fstr_Zuordnung_TypeClass extends Zeiger_TypeClass {
} // ID_BUE_WS_Fstr_Zuordnung_TypeClass
