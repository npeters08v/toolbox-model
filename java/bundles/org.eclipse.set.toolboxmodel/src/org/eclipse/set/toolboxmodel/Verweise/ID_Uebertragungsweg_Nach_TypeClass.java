/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Uebertragungsweg Nach Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Uebertragungsweg_Nach_TypeClass()
 * @model extendedMetaData="name='TCID_Uebertragungsweg_Nach' kind='elementOnly'"
 * @generated
 */
public interface ID_Uebertragungsweg_Nach_TypeClass extends Zeiger_TypeClass {
} // ID_Uebertragungsweg_Nach_TypeClass
