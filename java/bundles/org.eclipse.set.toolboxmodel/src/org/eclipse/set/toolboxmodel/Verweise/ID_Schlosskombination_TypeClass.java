/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Schlosskombination Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Schlosskombination_TypeClass()
 * @model extendedMetaData="name='TCID_Schlosskombination' kind='elementOnly'"
 * @generated
 */
public interface ID_Schlosskombination_TypeClass extends Zeiger_TypeClass {
} // ID_Schlosskombination_TypeClass
