/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package;
import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Zp7;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.impl.Signalbegriff_ID_TypeClassImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Zp7</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Zp7Impl extends Signalbegriff_ID_TypeClassImpl implements Zp7 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Zp7Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Signalbegriffe_Ril_301Package.eINSTANCE.getZp7();
	}

} //Zp7Impl
