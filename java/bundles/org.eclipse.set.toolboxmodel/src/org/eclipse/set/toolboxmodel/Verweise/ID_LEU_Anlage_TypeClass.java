/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID LEU Anlage Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_LEU_Anlage_TypeClass()
 * @model extendedMetaData="name='TCID_LEU_Anlage' kind='elementOnly'"
 * @generated
 */
public interface ID_LEU_Anlage_TypeClass extends Zeiger_TypeClass {
} // ID_LEU_Anlage_TypeClass
