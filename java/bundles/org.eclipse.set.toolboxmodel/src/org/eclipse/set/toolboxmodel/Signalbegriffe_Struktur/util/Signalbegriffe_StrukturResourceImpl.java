/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.util.Signalbegriffe_StrukturResourceFactoryImpl
 * @generated
 */
public class Signalbegriffe_StrukturResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Signalbegriffe_StrukturResourceImpl(URI uri) {
		super(uri);
	}

} //Signalbegriffe_StrukturResourceImpl
