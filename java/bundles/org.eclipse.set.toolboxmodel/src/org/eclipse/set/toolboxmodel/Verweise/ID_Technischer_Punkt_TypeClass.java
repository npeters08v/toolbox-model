/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Technischer Punkt Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Technischer_Punkt_TypeClass()
 * @model extendedMetaData="name='TCID_Technischer_Punkt' kind='elementOnly'"
 * @generated
 */
public interface ID_Technischer_Punkt_TypeClass extends Zeiger_TypeClass {
} // ID_Technischer_Punkt_TypeClass
