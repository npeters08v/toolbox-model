/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_Fla_Schutz_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID Fla Schutz Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_Fla_Schutz_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_Fla_Schutz_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_Fla_Schutz_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_FLA_SCHUTZ_TYPE_CLASS;
	}

} //ID_Fla_Schutz_TypeClassImpl
