/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID ZLV Bus Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_ZLV_Bus_TypeClass()
 * @model extendedMetaData="name='TCID_ZLV_Bus' kind='elementOnly'"
 * @generated
 */
public interface ID_ZLV_Bus_TypeClass extends Zeiger_TypeClass {
} // ID_ZLV_Bus_TypeClass
