/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.OzICE;
import org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.impl.Signalbegriff_ID_TypeClassImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Oz ICE</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OzICEImpl extends Signalbegriff_ID_TypeClassImpl implements OzICE {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OzICEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Signalbegriffe_Ril_301Package.eINSTANCE.getOzICE();
	}

} //OzICEImpl
