/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Unterbringung Technik Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Unterbringung_Technik_TypeClass()
 * @model extendedMetaData="name='TCID_Unterbringung_Technik' kind='elementOnly'"
 * @generated
 */
public interface ID_Unterbringung_Technik_TypeClass extends Zeiger_TypeClass {
} // ID_Unterbringung_Technik_TypeClass
