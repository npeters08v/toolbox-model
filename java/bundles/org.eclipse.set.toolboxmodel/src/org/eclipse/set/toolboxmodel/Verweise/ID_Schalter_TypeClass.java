/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Schalter Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Schalter_TypeClass()
 * @model extendedMetaData="name='TCID_Schalter' kind='elementOnly'"
 * @generated
 */
public interface ID_Schalter_TypeClass extends Zeiger_TypeClass {
} // ID_Schalter_TypeClass
