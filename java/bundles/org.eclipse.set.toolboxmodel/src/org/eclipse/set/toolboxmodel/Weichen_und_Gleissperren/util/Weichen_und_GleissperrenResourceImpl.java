/**
 */
package org.eclipse.set.toolboxmodel.Weichen_und_Gleissperren.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.set.toolboxmodel.Weichen_und_Gleissperren.util.Weichen_und_GleissperrenResourceFactoryImpl
 * @generated
 */
public class Weichen_und_GleissperrenResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public Weichen_und_GleissperrenResourceImpl(URI uri) {
		super(uri);
	}

} //Weichen_und_GleissperrenResourceImpl
