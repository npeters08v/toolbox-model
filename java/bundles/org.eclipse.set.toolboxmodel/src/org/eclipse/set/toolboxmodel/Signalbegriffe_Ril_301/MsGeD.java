/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ms Ge D</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getMsGeD()
 * @model extendedMetaData="name='Ms_ge_D' kind='elementOnly'"
 * @generated
 */
public interface MsGeD extends Signalbegriff_ID_TypeClass {
} // MsGeD
