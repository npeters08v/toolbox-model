/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID FT Anschaltbedingung Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_FT_Anschaltbedingung_TypeClass()
 * @model extendedMetaData="name='TCID_FT_Anschaltbedingung' kind='elementOnly'"
 * @generated
 */
public interface ID_FT_Anschaltbedingung_TypeClass extends Zeiger_TypeClass {
} // ID_FT_Anschaltbedingung_TypeClass
