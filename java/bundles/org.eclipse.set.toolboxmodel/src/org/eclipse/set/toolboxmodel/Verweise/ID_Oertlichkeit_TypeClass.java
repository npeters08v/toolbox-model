/**
 */
package org.eclipse.set.toolboxmodel.Verweise;

import org.eclipse.set.toolboxmodel.BasisTypen.Zeiger_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ID Oertlichkeit Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Verweise.VerweisePackage#getID_Oertlichkeit_TypeClass()
 * @model extendedMetaData="name='TCID_Oertlichkeit' kind='elementOnly'"
 * @generated
 */
public interface ID_Oertlichkeit_TypeClass extends Zeiger_TypeClass {
} // ID_Oertlichkeit_TypeClass
