/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hl3a</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getHl3a()
 * @model extendedMetaData="name='Hl_3a' kind='elementOnly'"
 * @generated
 */
public interface Hl3a extends Signalbegriff_ID_TypeClass {
} // Hl3a
