/**
 */
package org.eclipse.set.toolboxmodel.Verweise.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.set.toolboxmodel.BasisTypen.impl.Zeiger_TypeClassImpl;

import org.eclipse.set.toolboxmodel.Verweise.ID_Unterbringung_Technik_TypeClass;
import org.eclipse.set.toolboxmodel.Verweise.VerweisePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ID Unterbringung Technik Type Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ID_Unterbringung_Technik_TypeClassImpl extends Zeiger_TypeClassImpl implements ID_Unterbringung_Technik_TypeClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ID_Unterbringung_Technik_TypeClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VerweisePackage.Literals.ID_UNTERBRINGUNG_TECHNIK_TYPE_CLASS;
	}

} //ID_Unterbringung_Technik_TypeClassImpl
