/**
 */
package org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301;

import org.eclipse.set.toolboxmodel.Signalbegriffe_Struktur.Signalbegriff_ID_TypeClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bue02 Lp</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.set.toolboxmodel.Signalbegriffe_Ril_301.Signalbegriffe_Ril_301Package#getBue02Lp()
 * @model extendedMetaData="name='Bue_0_2Lp' kind='elementOnly'"
 * @generated
 */
public interface Bue02Lp extends Signalbegriff_ID_TypeClass {
} // Bue02Lp
